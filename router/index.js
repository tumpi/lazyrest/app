/**
 * Created by muharizals on 17/11/2017.
 */
const CollectionController = require('../controller/Collections');
let errors = require('restify-errors');
const Model = require('../model');
const Util = require('../util');

class Router{
    configure(server,io){

        let trimCollectionName = (req,res,next)=>{
            if(req.params.hasOwnProperty('collection_name')){
                req.params.collection_name = req.params.collection_name.toString().trim().toLowerCase().replace(/ /g, '');
            }
            next();
        };

        let checkProjectKey = (req,res,next)=>{
          if(req.params.hasOwnProperty('project_key')){
              Model.Project.findOne({_secret_key:req.params.project_key},(err,project)=>{
                  if(err){
                      return res.send(new errors.makeErrFromCode(500,err.message))
                  }
                  if(Util.isEmpty(project)){
                      return res.send(new errors.makeErrFromCode(404,"Project key not found"))
                  }
                  req.params._author = project._author;
                  req.params._project = project._id;
                  req.params._socket = project._socket;


                  if(project._secure && project._access_token){
                      if(!req.headers.hasOwnProperty("api-token")){
                          return res.send(new errors.makeErrFromCode(401,"API-Token header required"))
                      }

                      if(project._access_token.toString() == req.headers["api-token"]){
                          next();
                      }else{
                          return res.send(new errors.makeErrFromCode(401,"invalid Api-Token header"))
                      }
                  }else{
                      next();
                  }
              });
          }else{
              next();
          }
        };

        let getSubdomainName= (req,res,next)=>{
            req.params.project_key = Util.getKetFromString(".lazyrest.arizalsaputro.me", req.headers.host);
            if(req.params.project_key == -1){
                console.log(req.params.project_key);
                res.send(new errors.makeErrFromCode(401,"project key not found"))
            }else{
                next();
            }
        };


        server.use(getSubdomainName);
        server.use(trimCollectionName);
        server.use(checkProjectKey);

        server.get('/',(req,res)=>{
            res.send({
                name:'LazyRestApi',
                author:'arizalsaputro',
                description:'lazy rest api for testing/prototyping/learning',
            })
        });

        server.get('/:collection_name/search',CollectionController.search);
        server.get('/:collection_name/is_exist',CollectionController.isExist);
        server.get('/:collection_name/:id',CollectionController.getSingleRow);
        server.get('/:collection_name',CollectionController.getListCollection);

        server.post('/:collection_name',CollectionController.create);
        server.put('/:collection_name/:id',CollectionController.update);
        server.del('/:collection_name/:id',CollectionController.deleteSingleRow);
        server.del('/:collection_name',CollectionController.deleteCollection);

        ///server.del('/:project_key',CollectionController.deleteProject);

    }
}

module.exports = new Router();