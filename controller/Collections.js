/**
 * Created by muharizals on 17/11/2017.
 */

const Model = require('../model');
let errors = require('restify-errors');
const Util = require('../util');
class Collections{



    create(req,res){

        Model.Collection.create({
            _project:req.params._project,
            _author:req.params._author,
            _name:req.params.collection_name,
            _data:req.body
        },(err,doc)=>{
            if(err){
                return res.send(new errors.makeErrFromCode(500,err.message))
            }

            let nDoc = Object.assign({},doc._data);
            nDoc.id = doc._id;
            nDoc.createdAt = doc.createdAt;
            nDoc.updatedAt = doc.updatedAt;

            res.send(201,nDoc);
            Model.Project.addCollection(req.params);
        });
    }


    update(req,res){
        let query = {
            _id:req.params.id,
            _project:req.params._project,
            _author:req.params._author,
            _name:req.params.collection_name
        };

        Model.Collection.findOne(query,(err,doc)=>{
           if(err){
               return res.send(new errors.makeErrFromCode(500,err.message))
           }
           if(Util.isEmpty(doc)){
               return res.send(new errors.makeErrFromCode(404,"data key not found,invalid id"))
           }

            if(typeof req.body === 'object'){
                for(let obj in req.body){
                    if(req.body.hasOwnProperty(obj)){
                       doc._data[obj] = req.body[obj]
                    }
                }
            }else{
                doc._data = req.body;
            }

            doc.markModified('_data');
            doc.save((err)=>{
                if(err){
                    return res.send(new errors.makeErrFromCode(500,err.message))
                }
                let nDoc = Object.assign({},doc._data);
                nDoc.id = doc._id;
                nDoc.createdAt = doc.createdAt;
                nDoc.updatedAt = doc.updatedAt;
                res.send(nDoc);
            })
        });
    }

    deleteSingleRow(req,res){
        let query = {
            _id:req.params.id,
            _project:req.params._project,
            _author:req.params._author,
            _name:req.params.collection_name
        };

        Model.Collection.findOneAndRemove(query,{_data:req.body},(err,doc)=>{
            if(err){
                return res.send(new errors.makeErrFromCode(500,err.message))
            }
            if(Util.isEmpty(doc)){
                return res.send(new errors.makeErrFromCode(404,"data key not found,invalid id"))
            }
            let nDoc = Object.assign({},doc._data);
            nDoc.id = doc._id;
            nDoc.createdAt = doc.createdAt;
            nDoc.updatedAt = doc.updatedAt;

            res.send(nDoc);
            delete query._id;
            Model.Collection.count(query,(err,count)=>{
                if(!err && count == 0){
                    Model.Project.removeCollection(req.params);
                }
            });
        });
    }

    deleteCollection(req,res){
        let query = {
            _project:req.params._project,
            _author:req.params._author,
            _name:req.params.collection_name
        };

        Model.Collection.remove(query,(err,result)=>{
            if(err){
                return res.send(new errors.makeErrFromCode(500,err.message))
            }

            Model.Project.removeCollection(req.params);

            res.send(result);
        });
    }



    isExist(req,res){
        if(!req.query.hasOwnProperty("field") || !req.query.hasOwnProperty("value")){
            return res.send(new errors.makeErrFromCode(400,"parameter field and value are required"));
        }
        let query = {
            _project:req.params._project,
            _author:req.params._author,
            _name:req.params.collection_name
        };

        try{
            req.query.value = JSON.parse(req.query.value);
            console.log('with field')
        }catch (err){
            //console.log(err)
        }

        query[`_data.${req.query.field}`] = req.query.value;

        Model.Collection.count(query,(err,count)=>{
            if(err){
                return res.send(new errors.makeErrFromCode(500,err.message))
            }
            if(count>0){
                res.send({status:true})
            }else{
                res.send({status:false})
            }
        });
    }


    getSingleRow(req,res){
        let query = {
            _id:req.params.id,
            _project:req.params._project,
            _author:req.params._author,
            _name:req.params.collection_name
        };
        Model.Collection.findOne(query,(err,doc)=>{
            if(err){
                return res.send(new errors.makeErrFromCode(500,err.message))
            }
            if(Util.isEmpty(doc)){
                return res.send(new errors.makeErrFromCode(404,"data key not found,invalid id"))
            }
            let nDoc = Object.assign({},doc._data);
            nDoc.id = doc._id;
            nDoc.createdAt = doc.createdAt;
            nDoc.updatedAt = doc.updatedAt;
            res.send(nDoc);
        });
    }

    search(req,res){
        if(!req.query.hasOwnProperty("field") || !req.query.hasOwnProperty("value")){
            return res.send(new errors.makeErrFromCode(400,"parameter field and value are required"));
        }
        try{
            let options = Util.getOptions(req);
            let query = {
                _project:req.params._project,
                _author:req.params._author,
                _name:req.params.collection_name
            };

            query[`_data.${req.query.field}`] = { $regex: req.query.value.toString(), $options: 'i'}

            Model.Collection.find(query)
                .sort(options.sort)
                .skip(options.skip)
                .limit(options.limit)
                .lean()
                .exec((err,doc)=>{
                    if(err){
                        return res.send(new errors.makeErrFromCode(500,err.message))
                    }
                    let response = {
                        meta:{
                            current_page:options.page,
                            total_data:0,
                            limit:options.limit,
                            sort:options.sort,
                            offset:options.offset
                        },
                        data:[]
                    };
                    response.data = doc.map((item)=>{
                        let nDoc = Object.assign({},item._data);
                        nDoc.id = item._id;
                        nDoc.createdAt = item.createdAt;
                        nDoc.updatedAt = item.updatedAt;
                        return nDoc;
                    });
                    Model.Collection.count(query,(err,count)=>{
                        if(err){
                            return res.send(new errors.makeErrFromCode(500,err.message))
                        }
                        response.meta.total_data = count;
                        res.send(response);
                    });
                });

        }catch (err){
            res.send(err);
        }
    }

    getListCollection(req,res){
        try{
            let options = Util.getOptions(req);
            let query = {
                _project:req.params._project,
                _author:req.params._author,
                _name:req.params.collection_name
            };


            if(req.query.hasOwnProperty("field") && req.query.hasOwnProperty("value")){
                try{
                    req.query.value = JSON.parse(req.query.value)
                    console.log('with field')
                }catch (err){
                   // console.log(err)
                }
                query[`_data.${req.query.field}`] = req.query.value
            }

            Model.Collection.find(query)
                .sort(options.sort)
                .skip(options.skip)
                .limit(options.limit)
                .lean()
                .exec((err,doc)=>{
                    if(err){
                        return res.send(new errors.makeErrFromCode(500,err.message))
                    }
                    let response = {
                        meta:{
                            current_page:options.page,
                            total_data:0,
                            limit:options.limit,
                            sort:options.sort,
                            offset:options.offset
                        },
                        data:[]
                    };
                    response.data = doc.map((item)=>{
                        let nDoc = Object.assign({},item._data);
                        nDoc.id = item._id;
                        nDoc.createdAt = item.createdAt;
                        nDoc.updatedAt = item.updatedAt;
                        return nDoc;
                    });
                    Model.Collection.count(query,(err,count)=>{
                        if(err){
                            return res.send(new errors.makeErrFromCode(500,err.message))
                        }
                        response.meta.total_data = count;
                        res.send(response);
                    });
                });

        }catch (err){
            res.send(err);
        }
    }
}

module.exports = new Collections();