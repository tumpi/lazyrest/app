/**
 * Created by muharizals on 17/11/2017.
 */
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator')

const dbUser = new Schema({
    name:String,
    email:{type:String,index:true,unique:true,sparse:true,lowercase:true,trim:true},
    unique_id:String,
    phone_number:{type:String,index:true,unique:true,sparse:true}
},{timestamps:Date});

dbUser.plugin(uniqueValidator);

module.exports = mongoose.model('User',dbUser);