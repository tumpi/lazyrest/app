/**
 * Created by muharizals on 17/11/2017.
 */
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const uniqueValidator = require('mongoose-unique-validator')

const dbProject = new Schema({
    _author:{type:Schema.Types.ObjectId,ref:'User'},
    _project_name:String,
    _secret_key:{type:String,index:true,unique:true,sparse:true},
    _secure:{type:Boolean,default:false},
    _access_token:String,
    _collection:[String]
},{timestamps:Date});

dbProject.statics.addCollection = function({_project,_author,collection_name}){
    this.update({_id:_project,_author:_author},{$addToSet:{_collection:collection_name}},(err,result)=>{
       if(err){
           console.log('error adding collection ');
       }else{
           console.log('success adding collection');
       }
    });
};

dbProject.statics.removeCollection = function({_project,_author,collection_name}){
    this.update({_id:_project,_author:_author},{$pull:{_collection:collection_name}},(err,result)=>{
        if(err){
            console.log('error remove collection ');
        }else{
            console.log('success remove collection');
        }
    })
};


dbProject.plugin(uniqueValidator);

module.exports = mongoose.model('Project',dbProject);