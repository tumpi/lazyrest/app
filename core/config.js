module.exports= {
    db:{
        dbPath: process.env.MONGO_PATH || "mongodb://127.0.0.1:27017/lazyrestapi",
        db_options:{
            user:process.env.MONGO_DB_USER_NAME || 'lazyadmin',
            pass:process.env.MONGO_DB_PASS || 'admin123'
        }
    },
    port: 9234,
    timeZone: "Asia/Jakarta"
};

