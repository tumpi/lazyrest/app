/**
 * Created by muharizals on 18/11/2017.
 */
class MyUtil{
    isEmpty(obj){
        for(let prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }

        return true;
    }

    getKetFromString(key="",string = ""){
        let index = string.indexOf(key);
        if(index == -1){
            return -1;
        }
        return string.substring(0,index);
    }

    getOptions(req){
        let options = {
            sort:'-createdAt',
            limit:100,
            skip:0,
            page:1
        };
        if(req.query.hasOwnProperty('offset') && !isNaN(req.query.offset)){
            options.skip = parseInt(req.query.offset);
            options.page = parseInt((req.query.offset / 20) + 1);
            if(options.page <= 0){
                options.page = 1;
            }
        }
        if(req.query.hasOwnProperty('limit') && !isNaN(req.query.limit)){
            options.limit = parseInt(req.query.limit);
        }
        if(req.query.hasOwnProperty('sort')){
            options.sort = req.query.sort;
        }
        if(req.query.hasOwnProperty('limit') && req.query.limit == 0){
            throw {
                meta:{
                    current_page:options.page,
                    total_data:0,
                    limit:options.limit,
                    sort:options.sort,
                    offset:options.offset
                },
                data:[]
            };

        }
        if(req.query.hasOwnProperty('page') && !isNaN(req.query.page)){
            options.skip = parseInt((req.query.page * 20) - 20 );
            options.page = parseInt(req.query.page);
            options.limit = 20
        }
        if(options.skip < 0){
            options.skip = 0;
            options.page = 1;
        }
        return options;
        
    }
}

module.exports = new MyUtil();