const restify = require('restify')
let errors = require('restify-errors');
const mongoose = require('./core/mongoose')
const {timeZone,port} = require('./core/config')
const corsMiddleware = require('restify-cors-middleware')

const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['API-Token']
})


process.env.TZ = timeZone;

let server = restify.createServer({
    name: "lazyrestapi",
    version: "0.0.1"
});

const io = {}

server.pre(cors.preflight)
server.use(cors.actual)


server.use(restify.plugins.bodyParser());
server.use(restify.plugins.queryParser());
server.use((req,res,next)=>{
    try{
        try{
            if(req.body){
                req.body = JSON.parse(req.body);
            }
        }catch (err){

        }
        return next();
    }catch (err){
        console.log('disini');
        res.send(new errors.makeErrFromCode(400,err.message));
    }
})

server.listen(process.env.port || port,(err)=>{
    if(err){
        console.log('error:',err.message);
        process.exit(1);
    }
    console.log('server listening on port',process.env.PORT || port);
    mongoose.configure(server,io);
});

process.on('uncaughtException',(err)=> {
    if(err){
        console.log("LazyRestApiError",err);
    }
});